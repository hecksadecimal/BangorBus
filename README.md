# BangorBus
Pebble app and serverside stuff for Bangor Bus routes

Uses Python3 with BeautifulSoup for serverside shenanigans (grabs bus routes from bangormaine.gov and JSON-ifies them) and Pebble.js for the client app
